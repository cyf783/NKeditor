# NKeditor
NKedtior是基于 kindeditor 进行二次开发的项目
kindeditor 是一款优秀的开源在线编辑器。轻量级且功能强大，代码量却不到百度的ueditor编辑器的一半。可惜已经4年没有更新了.

RockYang大佬在kindeditor的基础上开发了 NKeditor, 做了以下调整：
1. 调整编辑器和弹出 dialog 的样式，美化了UI
2. 重写图片上传和批量图片上传插件，使用 html5 上传代替了 flash,实现了待上传图片预览，优化用户体验
3. 修复一些已知的bug，如 ajax 提交无法获取内容等
4. 新增涂鸦等功能

再次感谢 kindeditor 的开发者及大佬RockYang，为我们提供了如此优秀的在线编辑器。

我个人在大佬们的基于上做了以下修改：

1. 代码插件使用highlight
2. 所有类型的上传文件统一简化了，一步上传，去云支持(java代码示例)。
3. 支持截图粘贴上传（来源：zui。感谢！）
4. 表格table仿ueditor，支持拖拽选择单元格操作（来源：zui。感谢！）
5. 国际化文件只保留了中英繁。
6. 超链接加入标题修改
7. 工具栏只显示一行，超出部分点击更多显示（默认皮肤不支持）
8. 修复一些已知的bug（如文本不自动同步、编号行双回车后焦点丢失等）

# 部署和构建
1. npm install -g grunt-cli
2. 切换到 NKeditor 根目录，执行 npm install
3. 编译 : 执行 grunt
4. 如果要打包的话，执行 grunt zip，就会把编辑器的有关的的文件全部打包放入 dist 文件夹中，解压之后你就会得到一个干净的编辑器了。直接访问 index.html 进行预览。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/144725_f1ef0982_405607.png "demo.png")