/* ========================================================================
 * Kindeditor plugin - goto
 * ========================================================================
 * Copyright (c) 2019-2019 cnezsoft.com; Licensed MIT
 * ======================================================================== */

KindEditor.plugin('goto', function(K) {
    var self = this;
    self.afterCreate(function() {
        $(self.cmd.doc).on("click","a",function(e){
            window.open($(e.target).attr("href"))
            return false;
        });
    });
});