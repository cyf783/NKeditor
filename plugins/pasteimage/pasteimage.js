/* ========================================================================
 * Kindeditor plugin - paste-image
 * ========================================================================
 * Copyright (c) 2019-2019 cnezsoft.com; Licensed MIT
 * ======================================================================== */

KindEditor.plugin('pasteimage', function(K) {
    var self = this, name = 'pasteimage',
    lang = self.lang(name + '.');
    self.afterCreate(function() {
        var edit    = self.edit;
        var doc     = edit.doc;
        var uuid    = self.uuid;
        var options = self.options.pasteImage;
        if (options==false) {
            return;
        }
        if (typeof options === 'string') {
            options = {postUrl: options};
        }else{
            options = {postUrl: self.uploadJson};
        }
        $.extend({
            placeholder: placeholder
        }, options);
        if(!K.WEBKIT && !K.GECKO)
        {
            $(doc.body).on('keyup.ke' + uuid, function(ev)
            {
                if(ev.keyCode == 86 && ev.ctrlKey)
                    K.options.errorMsgHandler(lang.notSupportMsg, "error");
            });
        }

        if(self.setPlaceholder)
        {
            var placeholder = options.placeholder;
            if (placeholder === true) placeholder = lang.placeholder;
            if (placeholder) {
                var oldPlaceholder = self.getPlaceholder();
                if (!oldPlaceholder) oldPlaceholder = placeholder;
                else if (oldPlaceholder.indexOf(placeholder) < 0) placeholder = oldPlaceholder + '\n' + placeholder;
                self.setPlaceholder(placeholder);
            }
        }

        var pasteBegin = function() {
            // if ($.enableForm) {
            //     $.enableForm(false, 0, 1);
            //     $('body').one('click.ke' + uuid, function(){$.enableForm(true);});
            // }
            if (options.beforePaste) {
                options.beforePaste();
            }
            var imageLoadingEle = '<div class="image-loading-ele small" style="padding: 5px; background: #FFF3E0; width: 300px; border-radius: 2px; border: 1px solid #FF9800; color: #ff5d5d; margin: 10px 0;"><i class="icon icon-spin icon-spinner-indicator muted"></i> ' + lang.uploadingHint + '</div>';
            edit.cmd.inserthtml(imageLoadingEle);
            self.readonly(true);
        };

        var pasteEnd = function(error) {
            if(error) {
                if (options.onError) {
                    options.onError(error);
                } else {
                    if(error === true) error = lang.failMsg;
					K.options.errorMsgHandler(error, "error");
                }
            }
            // if ($.enableForm) {
            //     $.enableForm(true, 0, 1);
            // }
            // $('body').off('.ke' + uuid);
            if (options.afterPaste) {
                options.afterPaste();
            }
            $(doc.body).find('.image-loading-ele').remove();
            self.readonly(false);
        };

        var pasteUrl = options.postUrl;
        $(doc.body).on('paste.ke' + uuid, function(ev) {
            if (K.WEBKIT) {
                /* Paste in chrome.*/
                /* Code reference from http://www.foliotek.com/devblog/copy-images-from-clipboard-in-javascript/. */
                var original = ev.originalEvent;
                var clipboardItems = original.clipboardData && original.clipboardData.items;
                var clipboardItem = null;
                if(clipboardItems) {
                    var IMAGE_MIME_REGEX = /^image\/(p?jpeg|gif|png)$/i;
                    for (var i = 0; i < clipboardItems.length; i++)
                    {
                        if (IMAGE_MIME_REGEX.test(clipboardItems[i].type))
                        {
                            clipboardItem = clipboardItems[i];
                            break;
                        }
                    }
                }
                var file = clipboardItem && clipboardItem.getAsFile();
                if (!file) return;
                original.preventDefault();
                pasteBegin();

                var reader = new FileReader();
                reader.onload = function(evt) {
                    var result = evt.target.result;
                    $.post(pasteUrl, {
                        base64Data : result,
                        fileType : "image"
                    }, function(res){
                        if (res.code == "200") {
                            self.exec('insertimage', res.url);
                        } else {
                            K.options.errorMsgHandler(lang.failMsg, "error");
                        }
                        pasteEnd();
                    }, "json").error(function(){
                        pasteEnd(true);
                    });
                };
                reader.readAsDataURL(file);
            } else {
                /* Paste in firefox and other browsers. */
                setTimeout(function() {
                    var html = K(doc.body).html();
                    if(html.search(/<img src="data:.+;base64,/) > -1) {
                        pasteBegin();
                        $.post(pasteUrl, {editor: html}, function(data) {
                            if(data.indexOf('<img') === 0) data = '<p>' + data + '</p>';
                            edit.html(data);
                            pasteEnd();
                        }).error(function()
                        {
                            pasteEnd(true);
                        });
                    }
                }, 80);
            }
        });

        self.beforeRemove(function() {
            $(doc.body).off('.ke' + uuid);
        });
    });
});