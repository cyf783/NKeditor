
function NkUpload(options) {
	this.init(options);
}
_extend(NkUpload, {
	kEditor:null,
	init : function(options) {
		this.kEditor = options.kEditor;
		var self = this,
			container = K(this.kEditor.srcElement[0]),
			fieldName = options.fieldName || 'nkFile',
			url = options.url || '',
			extraParams = options.extraParams || {},
			target = options.target || 'kindeditor_upload_iframe_' + new Date().getTime();
		options.afterError = options.afterError || function(str) {
				K.options.errorMsgHandler(str, "error");
		};

		var hiddenElements = [];
		for(var k in extraParams){
			hiddenElements.push('<input type="hidden" name="' + k + '" value="' + extraParams[k] + '" />');
		}

		var html = [
			'<div class="ke-inline-block">',
			(options.target ? '' : '<iframe name="' + target + '" style="display:none;"></iframe>'),
			(options.form ? '<div class="ke-upload-area">' : '<form class="ke-upload-area ke-form" style="display:none;" method="post" enctype="multipart/form-data" target="' + target + '" action="' + url + '">'),
			'<span class="ke-button-common">',
			hiddenElements.join(''),
			'</span>',
			'<input type="file" class="ke-upload-file" style="display:none;" name="' + fieldName + '" tabindex="-1" />',
			(options.form ? '</div>' : '</form>'),
			'</div>'].join('');

		var div = K(html, container.doc);
		container.before(div);

		self.div = div;
		self.iframe = options.target ? K('iframe[name="' + target + '"]') : K('iframe', div);
		self.form = options.form ? K(options.form) : K('form', div);
		self.fileBox = K('.ke-upload-file', div);
		var width = options.width || K('.ke-button-common', div).width();
		K('.ke-upload-area', div).width(width);
		self.options = options;
	},
	submit : function() {
		var self = this,
			iframe = self.iframe;
		iframe.bind('load', function() {
			iframe.unbind();
			try {
				var fileValue = self.fileBox.val();
				var fileName = fileValue.substring(fileValue.lastIndexOf("\\")+1);  
				// 清空file
				var tempForm = document.createElement('form');
				self.fileBox.before(tempForm);
				K(tempForm).append(self.fileBox);
				tempForm.reset();
				K(tempForm).remove(true);

				var doc = K.iframeDoc(iframe),
					pre = doc.getElementsByTagName('pre')[0],
					str = '', data;
				if (pre) {
					str = pre.innerHTML;
				} else {
					str = doc.body.innerHTML;
				}
				// Bugfix: https://github.com/kindsoft/kindeditor/issues/81
				str = _unescape(str);
				// Bugfix: [IE] 上传图片后，进度条一直处于加载状态。
				iframe[0].src = 'javascript:false';
				data = K.json(str);
				data["fileName"]=fileName;
			} catch (e) {
				self.options.afterError.call(self, e.code+":"+e.message);
			}
			if (data) {
				self.options.afterUpload.call(self, data);
			}
			self.remove();
		});
		self.form[0].submit();
		var LoadingEle = '<div class="file-loading-ele small" style="padding: 5px; background: #FFF3E0; width: 100%; border-radius: 2px; border: 1px solid #FF9800; color: #ff5d5d; margin: 10px 0;"><i class="icon icon-spin icon-spinner-indicator muted"></i> ' + self.kEditor.lang('uploadLoading') + '</div>';
		self.kEditor.edit.cmd.inserthtml(LoadingEle);
		self.kEditor.readonly(true);
		return self;
	},
	remove : function() {
		var self = this;
		$(self.kEditor.edit.doc.body).find('.file-loading-ele').remove();
		self.kEditor.readonly(false);
		if (self.fileBox) {
			self.fileBox.unbind();
		}
		// Bugfix: [IE] 上传图片后，进度条一直处于加载状态。
		//self.iframe[0].src = 'javascript:false';
		self.iframe.remove();
		self.div.remove();
		return self;
	}
});

function _nkupload(options) {
	return new NkUpload(options);
}

K.NkUploadClass = NkUpload;
K.nkupload = _nkupload;

